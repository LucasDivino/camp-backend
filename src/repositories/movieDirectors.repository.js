const { MovieDirector } =  require('../models')

module.exports =  {
  list: (query) => MovieDirector.findAndCountAll(query),
  getById: (id) => MovieDirector.findByPk(id),
  get: (params) => MovieDirector.findOne(params),
  create: (params) => MovieDirector.create(params),
  update: (movieDirector) => movieDirector.save(),
  destroy: (id) => MovieDirector.destroy({ where: { id } }),
};