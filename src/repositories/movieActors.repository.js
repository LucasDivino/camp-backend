const { MovieActors } =  require('../models')

module.exports =  {
  list: (query) => MovieActors.findAndCountAll(query),
  getById: (id) => MovieActors.findByPk(id),
  get: (params) => MovieActors.findOne(params),
  create: async (params) => MovieActors.create(params),
  update: (movieActors) => movieActors.save(),
  destroy: (id) => MovieActors.destroy({ where: { id } }),
};