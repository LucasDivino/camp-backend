const usersRepository = require('./users.repository')
const actorsRepository = require('./actors.repository') 
const directorsRepository = require('./directors.repository')
const movieActorsRepository = require('./movieActors.repository')
const movieDirectorsRepository = require('./movieDirectors.repository')
const votesRepository = require('./votes.repository')
const moviesRepository = require('./movies.repository')

module.exports = {
  usersRepository,
  actorsRepository,
  directorsRepository,
  movieActorsRepository,
  movieDirectorsRepository,
  votesRepository,
  moviesRepository
};
