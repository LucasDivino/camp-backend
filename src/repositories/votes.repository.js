const { Vote } =  require('../models')

module.exports =  {
  list: (query) => Vote.findAndCountAll(query),
  getById: (id) => Vote.findByPk(id),
  get: (params) => Vote.findOne(params),
  create: async (params) => Vote.create(params),
  update: (votes) => votes.save(),
  destroy: (id) => Vote.destroy({ where: { id } }),
};