const { Movie } =  require('../models')

module.exports =  {
  list: (query) => Movie.findAndCountAll(query),
  getById: (id, includes) => Movie.findByPk(id, includes),
  get: (params) => Movie.findOne(params),
  create: (params) => Movie.create(params),
  update: (Movie) => Movie.save(),
  destroy: (id) => Movie.destroy({ where: { id } }),
};