const { votesRepository  } = require("../../repositories");

module.exports.list = async (options) => {
  const { count, rows } = await votesRepository .list(options);

  return {
    metadata: {
      total: count,
    },
    data: rows,
  };
};
