const { votesRepository  } = require("../../repositories");

module.exports.update = async (id, body) => {

  const vote = await votesRepository .findById(id)

  if (!vote) {
    throw {
      status: 404,
      message: 'vote not found',
    };
  }

  Object.keys(body).forEach((key) => {
    book.setDataValue(key, validated[key]);
  })

  return await votesRepository.update(vote);
};
