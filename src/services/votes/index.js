const { list } = require('./list')
const { create } = require('./create')
const { get } = require('./get')
const { deleteOne } = require('./delete')
const { update } = require('./update')

module.exports = {
  list,
  create,
  get,
  update,
  deleteOne
};