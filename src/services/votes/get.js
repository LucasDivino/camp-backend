const { votesRepository  } = require("../../repositories");

module.exports.get = async (id) => {
  return await votesRepository .getById(id);
};
