const { votesRepository  } = require("../../repositories");
const { assert, object, string, number, nullable } = require('superstruct')

module.exports.create = async (vote) => {

  const Vote = object({
    score: number(),
    description: nullable(string()),
    user_id: number(),
    movie_id: number(),
  })
  
  assert(vote, Vote)

  if(vote.score > 5 || vote.score < 0){
    throw Error('Score should be between 0 and 5')
  }

  return await votesRepository.create(vote);
};
