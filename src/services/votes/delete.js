const { votesRepository  } = require("../../repositories");

module.exports.deleteOne = async (id) => {
  const vote = await votesRepository .getById(id);

  if (!vote) {
    throw {
      status: 404,
      message: 'votes not found',
    };
  }

  return votesRepository.deleteOne(id);
};