const { moviesRepository } = require("../../repositories");

module.exports.deleteOne = async (id) => {
  const movie = await moviesRepository.getById(id);

  if (!movie) {
    throw {
      status: 404,
      message: 'movie not found',
    };
  }

  return moviesRepository.deleteOne(id);
};