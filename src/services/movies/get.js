const { moviesRepository } = require("../../repositories");
const { votesRepository } = require("../../repositories")

module.exports.get = async (movieId) => {
  const movie = await moviesRepository.getById(movieId, {
    include: [{association: 'directors'}, {association: 'actors'}]
  });

  const {count, rows} = await votesRepository.list({
    attributes: ['score'],
    where: {
      movie_id: movieId,
    }
  })

  const score = rows.reduce((accumulator, element) => accumulator + element.score, 0) / count
  
  movie.dataValues.score = score

  return movie
};
