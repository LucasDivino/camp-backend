const { moviesRepository } = require("../../repositories");
const { assert, object, string } = require('superstruct')

module.exports.create = async (movie) => {

  const Movie = object({
    name: string(),
    description: string(),
    releaseDate: string(), 
  })
  
  assert(movie, Movie)

  return await moviesRepository.create(movie);
};