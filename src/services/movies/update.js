const { moviesRepository } = require("../../repositories");

module.exports.update = async (id, body) => {

  const movie = await moviesRepository.findById(id)

  if (!movie) {
    throw {
      status: 404,
      message: 'movie not found',
    };
  }

  Object.keys(body).forEach((key) => {
    book.setDataValue(key, validated[key]);
  })

  return await moviesRepository.update(movie);

};
