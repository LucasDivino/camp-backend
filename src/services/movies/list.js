const { Op } = require('sequelize')
const { moviesRepository } = require("../../repositories");

module.exports.list = async (query) => {

  const makeOrQuery = (key, name) => query[key] && query[key].split(',').map((value) => {
    return {[name]: {[Op.like]: `%${value}%`} } 
  })

  const actors = makeOrQuery('actors', 'name')
  const directos = makeOrQuery('directors', 'name')

  const filter = {
    where: {
      name: {
        [Op.like]: `%${query.name || ''}`,
      }
    },
    include: [{
      association: 'actors',
      where : actors && { [Op.or]: actors}
    },
    {
      association: 'directors',
      where : directos && { [Op.or]: directos}
    }]
  }

  const { count, rows } = await moviesRepository.list(filter);

  return {
    metadata: {
      total: count,
    },
    data: rows,
  };
};
