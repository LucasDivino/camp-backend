const { actorsRepository } = require("../../repositories");

module.exports.deleteOne = async (id) => {
  const actor = await actorsRepository.getById(id);

  if (!actor) {
    throw {
      status: 404,
      message: 'actor not found',
    };
  }

  return actorsRepository.deleteOne(id);
};