const { actorsRepository } = require("../../repositories");

module.exports.update = async (id, body) => {

  const actor = await actorsRepository.findById(id)

  if (!actor) {
    throw {
      status: 404,
      message: 'actor not found',
    };
  }

  Object.keys(body).forEach((key) => {
    book.setDataValue(key, validated[key]);
  })

  return await actorsRepository.update(actor);

};
