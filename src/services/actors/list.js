const { actorsRepository } = require("../../repositories");

module.exports.list = async (options) => {

  const { count, rows } = await actorsRepository.list(options);

  return {
    metadata: {
      total: count,
    },
    data: rows,
  };
};
