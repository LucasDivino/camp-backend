const { actorsRepository } = require("../../repositories");

module.exports.get = async (userId) => {
  return await actorsRepository.getById(userId);
};
