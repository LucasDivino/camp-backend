const { actorsRepository } = require("../../repositories");
const { assert, object, string } = require('superstruct')

module.exports.create = async (actor) => {

  const Actor = object({
    name: string(),
  })
  
  assert(actor, Actor)

  return await actorsRepository.create(actor);
};