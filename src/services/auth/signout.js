 
const { usersRepository } = require("../../repositories");

module.exports.signout = async (id) => {
  const user = await usersRepository.getById(id);

  user.setDataValue('token', null);

  return usersRepository.update(user);
};