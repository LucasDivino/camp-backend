const jwt = require("jsonwebtoken");
const { encryptor } = require("../../utils");
const { usersRepository } = require("../../repositories");
const { promisify } = require("util");
const config = require("../../config")

module.exports.signin = async (email, password) => {
    const user = await usersRepository.get({ email });
  
    if (!user) {
      throw {
        status: StatusCodes.NOT_FOUND,
        message: messages.notFound("user"),
      };
    }
  
    const valid = await encryptor.compare(password, user.password);

    if (!valid) {
      throw {
        status: 401,
        message: 'email or password incorrect',
      };
    }
  
    const payload = {
      id: user.id,
      email: user.email,
    };
  
    const sign = promisify(jwt.sign);
    const token = await sign(payload, config.session);

    user.setDataValue('token', token);

    await usersRepository.update(user);
  
    return { email, token };
};
