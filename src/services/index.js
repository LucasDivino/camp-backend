const usersServices = require('./users')
const authService = require('./auth')
const directorsServices = require('./directors')
const actorsServices = require('./actors')
const movieDirectorsServices = require('./movieDirectors')
const movieActorsServices = require('./movieActors')
const votesServices = require('./votes')
const moviesServices = require('./movies')

module.exports = {
    usersServices,
    authService,
    actorsServices,
    directorsServices,
    movieActorsServices,
    movieDirectorsServices,
    votesServices,
    moviesServices
}