const { movieActorsRepository } = require("../../repositories");

module.exports.list = async (options) => {
  const { count, rows } = await movieActorsRepository.list(options);

  return {
    metadata: {
      total: count,
    },
    data: rows,
  };
};
