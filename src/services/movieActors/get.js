const { movieActorsRepository } = require("../../repositories");

module.exports.get = async (id) => {
  return await movieActorsRepository.getById(id);
};
