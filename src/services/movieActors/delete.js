const { movieActorsRepository } = require("../../repositories");

module.exports.deleteOne = async (id) => {
  const movieActor = await movieActorsRepository.getById(id);

  if (!movieActor) {
    throw {
      status: 404,
      message: 'register not found',
    };
  }

  return movieActorsRepository.deleteOne(id);
};