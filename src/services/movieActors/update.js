const { movieActorsRepository } = require("../../repositories");

module.exports.update = async (id, body) => {

  const movieActors = await movieActorsRepository.findById(id)

  if (!movieActors) {
    throw {
      status: 404,
      message: 'register not found',
    };
  }

  Object.keys(body).forEach((key) => {
    book.setDataValue(key, validated[key]);
  })

  return await movieActorsRepository.update(movieActors);
};