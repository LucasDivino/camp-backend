const { movieActorsRepository } = require("../../repositories");
const { assert, object, string, number } = require('superstruct')

module.exports.create = async (movieActor) => {

  const MovieActor = object({
    movie_id: number(),
    actor_id: number()
  })
  
  assert(movieActor, MovieActor)

  console.log(movieActor)

  return await movieActorsRepository.create(movieActor);
};
