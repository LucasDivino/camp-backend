const { movieDirectorsRepository } = require("../../repositories");

module.exports.list = async (options) => {
  const { count, rows } = await movieDirectorsRepository.list(options);

  return {
    metadata: {
      total: count,
    },
    data: rows,
  };
};
