const { movieDirectorsRepository } = require("../../repositories");
const { assert, object, string, number } = require('superstruct')

module.exports.create = async (movieDirector) => {

  const MovieDirector = object({
    movie_id: number(),
    director_id: number()
  })
  assert(movieDirector, MovieDirector)

  return await movieDirectorsRepository.create(movieDirector);
};
