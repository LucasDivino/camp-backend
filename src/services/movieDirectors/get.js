const { movieDirectorsRepository } = require("../../repositories");

module.exports.get = async (id) => {
  return await movieDirectorsRepository.getById(id);
};
