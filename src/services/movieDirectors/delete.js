const { movieDirectorsRepository } = require("../../repositories");

module.exports.deleteOne = async (id) => {
  const movieDirector = await movieDirectorsRepository.getById(id);

  if (!movieDirector) {
    throw {
      status: 404,
      message: 'register not found',
    };
  }

  return movieDirectorsRepository.deleteOne(id);
};