const { movieDirectorsRepository } = require("../../repositories");

module.exports.update = async (id, body) => {

  const movieDirectors = await movieDirectorsRepository.findById(id)

  if (!movieDirectors) {
    throw {
      status: 404,
      message: 'director not found',
    };
  }

  Object.keys(body).forEach((key) => {
    book.setDataValue(key, validated[key]);
  })

  return await movieDirectorsRepository.update(movieDirectors);
};