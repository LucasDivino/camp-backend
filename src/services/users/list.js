const { usersRepository } = require("../../repositories");

module.exports.list = async (options) => {

  const { count, rows } = await usersRepository.list(options);
  console.log(count, rows)
  return {
    metadata: {
      total: count,
    },
    data: rows,
  };
};
