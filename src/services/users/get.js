const { usersRepository } = require("../../repositories");

module.exports.get = async (userId) => {
  return await usersRepository.getById(userId);
};
