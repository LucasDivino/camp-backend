const { usersRepository } = require("../../repositories");

module.exports.deleteOne = async (id) => {
  const user = await usersRepository.getById(id);

  if (!user) {
    throw {
      status: 404,
      message: 'user not found',
    };
  }

  user.setDataValue('isActive', false);

  return usersRepository.update(user);
};