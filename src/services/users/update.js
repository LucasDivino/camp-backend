const { usersRepository } = require("../../repositories");

module.exports.update = async (id, body) => {

  const user = await usersRepository.findById(id)

  if (!user) {
    throw {
      status: 404,
      message: 'user not found',
    };
  }

  Object.keys(body).forEach((key) => {
    book.setDataValue(key, validated[key]);
  })

  return await usersRepository.update(user);

};
