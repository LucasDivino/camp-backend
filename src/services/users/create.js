const { usersRepository } = require("../../repositories");
const { assert, object, string } = require('superstruct')

module.exports.create = async (userData) => {

  const User = object({
    name: string(),
    email: string(),
    password: string(),
  })
  
  assert(userData, User)

  const emailExists = await usersRepository.get({
    where: {
      email: userData.email
    }
  })

  if(emailExists) throw {
    status: 409,
    message: 'this email is already being used',
  };

  return await usersRepository.create(userData);
};
