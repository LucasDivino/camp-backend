const { directorsRepository } = require("../../repositories");

module.exports.list = async (options) => {
  console.log(options)
  const { count, rows } = await directorsRepository.list(options);

  return {
    metadata: {
      total: count,
    },
    data: rows,
  };
};
