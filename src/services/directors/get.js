const { directorsRepository } = require("../../repositories");

module.exports.get = async (id) => {
  return await directorsRepository.getById(id);
};
