const { directorsRepository } = require("../../repositories");

module.exports.update = async (id, body) => {

  const director = await directorsRepository.findById(id)

  if (!director) {
    throw {
      status: 404,
      message: 'director not found',
    };
  }

  Object.keys(body).forEach((key) => {
    book.setDataValue(key, validated[key]);
  })

  return await directorsRepository.update(director);
};
