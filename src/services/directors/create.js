const { directorsRepository } = require("../../repositories");
const { assert, object, string } = require('superstruct')

module.exports.create = async (director) => {

  const Director = object({
    name: string(),
  })
  
  assert(director, Director)

  return await directorsRepository.create(director);
};
