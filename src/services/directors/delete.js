const { directorsRepository } = require("../../repositories");

module.exports.deleteOne = async (id) => {
  const director = await directorsRepository.getById(id);

  if (!director) {
    throw {
      status: 404,
      message: 'director not found',
    };
  }

  return directorsRepository.deleteOne(id);
};