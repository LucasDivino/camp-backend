module.exports = (sequelize, DataTypes) => {

  const Director = sequelize.define(
    "Director",
    {
      name: DataTypes.STRING,
    },
    {
      tableName: "directors",
    }
  );

  Director.associate = (models) => {
    Director.belongsToMany(models.Movie, {foreignKey: 'movie_id', through: 'movie_directors', as: 'movies'} );
  }

  return Director;
      
}