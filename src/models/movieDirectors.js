module.exports =  (sequelize, DataTypes) => {
    const MovieActors = sequelize.define(
      "MovieDirector",
      {},
      {
        tableName: "movie_directors",
      }
    );

    MovieActors.associate = (models) => {
      MovieActors.belongsTo(models.Director, {foreignKey: 'director_id'});
      MovieActors.belongsTo(models.Movie, {foreignKey: 'movie_id'})
    }
  
    return MovieActors;
    
  }