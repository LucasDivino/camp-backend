const { encryptor } = require('../utils')

module.exports = (sequelize, DataTypes) => {
    
  const User = sequelize.define(
    "User",
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      name: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      isAdmin: DataTypes.BOOLEAN,    
      isActive: DataTypes.BOOLEAN,
      token: DataTypes.STRING
    },
    {
      tableName: "users",
    }
  );

  User.beforeSave(async (user) => {
    const password = await encryptor.hashPassword(user.password);
    if (user.changed("password")) {
      Object.assign(user, { password });
    }
    return user;
  })

  User.prototype.toJSON = function () {
    const user = { ...this.get() };
    return Object.fromEntries(
      Object.entries(user).filter(([key]) => !["password", "token"].includes(key))
    );
  };

  User.associate = (models) => {
    User.hasMany(models.Vote, {foreignKey: 'user_id'} );
  }

  return User;

}