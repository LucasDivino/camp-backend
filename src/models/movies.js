module.exports =  (sequelize, DataTypes) => {
  const Movie = sequelize.define(
    "Movie",
    {
      name: DataTypes.STRING,
      description: DataTypes.STRING,
      releaseDate: DataTypes.DATE 
    },
    {
      tableName: "movies",
    }
  );

  Movie.associate = (models) => {
    Movie.hasMany(models.Vote, {foreignKey: 'movie_id'} );
    Movie.belongsToMany(models.Actor, {foreignKey: 'actor_id', through: 'movie_actors', as: 'actors'} );
    Movie.belongsToMany(models.Director, {foreignKey: 'director_id', through: 'movie_directors', as: 'directors'} );
  }

  return Movie;
  
}