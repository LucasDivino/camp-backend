module.exports = (sequelize, DataTypes) => {

    const Votes = sequelize.define(
      "Vote",
      {
        score: DataTypes.INTEGER,
        description: DataTypes.STRING
      },
      {
        tableName: "votes",
      }
    )

    Votes.associate = (models) => {
        Votes.belongsTo(models.Movie, {foreignKey: 'movie_id'} );
        Votes.belongsTo(models.User, {foreignKey: 'user_id'});
    }
  
    return Votes;
        
}