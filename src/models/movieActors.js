module.exports =  (sequelize, DataTypes) => {
    const MovieActors = sequelize.define(
      "MovieActors",
      {},
      {
        tableName: "movie_actors",
      }
    );

    MovieActors.associate = (models) => {
        MovieActors.belongsTo(models.Actor, {foreignKey: 'actor_id'});
        MovieActors.belongsTo(models.Movie, {foreignKey: 'movie_id'})
    }
  
    return MovieActors;
    
  }