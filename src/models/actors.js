module.exports = (sequelize, DataTypes) => {

  const Actor = sequelize.define(
    "Actor",
    {
      name: DataTypes.STRING,
    },
    {
      tableName: "actors",
    }
  );

  Actor.associate = (models) => {
    Actor.belongsToMany(models.Movie, {foreignKey: 'movie_id', through: 'movie_actors', as: 'movies'} );
  }

  return Actor;
    
}