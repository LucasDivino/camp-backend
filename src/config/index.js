const dotenv = require('dotenv');
const database = require('./database')

dotenv.config();

module.exports = {
  port: process.env.PORT,
  environment: process.env.NODE_ENV,
  session: process.env.JWT_SECRET,
  database
};
