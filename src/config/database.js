
module.exports = {
  dialect: 'postgres',
  host: 'localhost',
  username: 'postgres',
  password: '123',
  database: 'camp',
  define: {
    timestamps: true,
    underscored: true
  }
}