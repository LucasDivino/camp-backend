const { json } = require('express');
const cors = require('cors');
const routes =  require('../router')

module.exports = async (app) => {
  app.use(cors());
  app.use(json());
  Object.keys(routes).forEach((key) => app.use(`/api/${key}`, routes[key]));
};
