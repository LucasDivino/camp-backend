const bcrypt = require('bcrypt')

module.exports = {
    hashPassword: (password) => bcrypt.hash(password, 8),
    compare: (password, userPassword) => bcrypt.compare(password, userPassword)
}