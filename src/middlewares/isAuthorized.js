const jwt = require("jsonwebtoken");
const { promisify } = require("util");
const { usersRepository } = require("../repositories");
const config = require('../config')

module.exports = async (req, res, next) => {
  try {
    let token;

    if (req.headers && req.headers.authorization) {
      const [scheme, credentials] = req.headers.authorization.split(" ");

      if (scheme.match(/^Bearer$/i)) {
        token = credentials;
      } else {
        throw {
          status: 401,
          message: 'invalid authorization format',
        };
      }
    } else {
      throw {
        status: 401,
        message: 'missing authorization',
      };
    }

    const verify = promisify(jwt.verify);
    const decoded = await verify(token, config.session);
    const user = await usersRepository.getById(decoded.id);

    if (!user) {
      throw {
        status: 404,
        message: 'user not found',
      };
    }

    req.session = { token, id: decoded.id, email: decoded.email };
    req.user = user;

    return next();
  } catch (error) {
    return res.status(error.status).json(error.message);
  }
};
