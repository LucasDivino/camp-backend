const router = require("express").Router();
const { moviesController } = require("../controllers");
const { isAuthorized } = require("../middlewares");

router.use(isAuthorized)

router.get("/", moviesController.list);
router.get('/:id', moviesController.find)
router.post("/", moviesController.create);
router.delete("/:id", moviesController.delete);
router.patch("/:id", moviesController.update)


module.exports.movies = router;