const router = require("express").Router();
const { movieDirectorsController } = require("../controllers");
const { isAuthorized } = require("../middlewares");

router.use(isAuthorized)

router.get("/", movieDirectorsController.list);
router.get('/:id', movieDirectorsController.find)
router.post("/", movieDirectorsController.create);
router.delete("/:id", movieDirectorsController.delete);
router.patch("/:id", movieDirectorsController.update)


module.exports.movieDirectors = router;