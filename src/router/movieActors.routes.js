const router = require("express").Router();
const { movieActorsController } = require("../controllers");
const { isAuthorized } = require("../middlewares");

router.use(isAuthorized)

router.get("/", movieActorsController.list);
router.get('/:id', movieActorsController.find)
router.post("/", movieActorsController.create);
router.delete("/:id", movieActorsController.delete);
router.patch("/:id", movieActorsController.update)


module.exports.movieActors = router;
