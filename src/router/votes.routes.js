const router = require("express").Router();
const { votesController } = require("../controllers");
const { isAuthorized } = require("../middlewares");

router.use(isAuthorized)

router.get("/", votesController.list);
router.get('/:id', votesController.find)
router.post("/", votesController.create);
router.delete("/:id", votesController.delete);
router.patch("/:id", votesController.update)


module.exports.votes = router;