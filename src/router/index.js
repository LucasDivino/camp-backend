const { users } = require("./users.routes");
const { auth } = require("./auth.routes")
const { directors } = require("./directos.routes")
const { actors } = require("./actors.routes")
const { movieActors} = require("./movieActors.routes")
const { movieDirectors } = require("./movieDirectors.routes")
const { votes } = require("./votes.routes")
const { movies } = require("./movies.routes")

module.exports = {
  users,
  auth,
  directors,
  actors,
  movieActors,
  movieDirectors,
  votes,
  movies
};
