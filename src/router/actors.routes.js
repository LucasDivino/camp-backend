const router = require("express").Router();
const { actorsController } = require("../controllers");
const { isAuthorized } = require("../middlewares");

router.use(isAuthorized)

router.get("/", actorsController.list);
router.get('/:id', actorsController.find)
router.post("/", actorsController.create);
router.delete("/:id", actorsController.delete);
router.patch("/:id", actorsController.update)


module.exports.actors = router;