const router = require("express").Router();
const { directorsController } = require("../controllers");
const { isAuthorized } = require("../middlewares");

router.use(isAuthorized)

router.get("/", directorsController.list);
router.get('/:id', directorsController.find)
router.post("/", directorsController.create);
router.delete("/:id", directorsController.delete);
router.patch("/:id", directorsController.update)


module.exports.directors = router;