const router = require("express").Router();
const { authController } = require("../controllers");
const { isAuthorized } = require("../middlewares");

router.post("/", authController.signin);
router.get('/', isAuthorized, authController.signout)


module.exports.auth = router;