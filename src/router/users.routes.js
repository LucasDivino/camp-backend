const router = require("express").Router();
const { usersController } = require("../controllers");
const { isAuthorized } = require("../middlewares");

router.use(isAuthorized)

router.get("/", usersController.list);
router.get('/:id', usersController.find)
router.post("/", usersController.create);
router.delete("/:id", usersController.delete);
router.patch("/:id", usersController.update)


module.exports.users = router;