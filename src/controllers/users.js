const { usersServices } = require('../services')

module.exports = {
    list: async (_, res) => {
        try {
            const response = await usersServices.list({});
            if (!response || response.data.length === 0) {
                return res.status(204).end();
            }
            return res.status(200).json(response);
        } catch (error) {
            return res
                .status(error.status || 500)
                .json({error: String(error)});
        }
    },
    create: async (req, res) => {
        try {
            const { body } = req
            if(!req.user.isAdmin) return res.status(401).json({
                message: 'missing authentication'
            });

            const response = await usersServices.create(body)
            return res.status(201).json(response);
            
        } catch (error) {
            return res
                .status(500)
                .json({error: String(error)});
        }
    },
    delete: async(req, res) => {
        try {
            const { id } = req.params
            const response = await usersServices.deleteOne(id)
            res.status(204).json(response)
        } catch (error){
            return res
                .status(error.status || 500)
                .json({error: String(error)});
        }
    },
    find: async(req, res) => {
        try {
            const { id } = req.params
            const response = await usersServices.get(id)
            res.status(200).json(response)
        } catch (error){
            return res
                .status(error.status || 500)
                .json({error: String(error)});
        }
    },
    update: async(req, res) => {
        try {
            const { id } = req.params
            const { body } = req
            const response = await usersServices.update(id, body)
            res.status(200).json(response)
        } catch (error){
            return res
                .status(error.status || 500)
                .json({error: String(error)});
        }
    },
    
};
  