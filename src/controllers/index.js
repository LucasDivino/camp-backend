const usersController = require('./users')
const authController = require('./auth')
const directorsController = require('./directors')
const actorsController = require('./actors')
const votesController = require('./votes')
const movieActorsController = require('./movieActors')
const movieDirectorsController = require('./movieDirectors')
const moviesController = require('./movies')

module.exports = {
    usersController,
    authController,
    directorsController,
    actorsController,
    votesController,
    movieActorsController,
    movieDirectorsController,
    moviesController
}