const { authService } = require("../services");

module.exports = {
  signin: async (req, res) => {
    try {
      const { email, password } = req.body;
      const response = await authService.signin(email, password);
      return res.status(200).json(response);
    } catch (error) {
      return res
        .status(500)
        .json(error.message);
    }
  },
  signout: async (req, res) => {
    try {
      await authService.signout(req.user.id);

      return res.status(200).json({
        success: true,
      });
    } catch (error) {
      return res.status(500).json({
        message: String(error),
      });
    }
  },
};
