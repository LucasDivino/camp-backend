const express = require('express');
const config = require('./config');
const loaders = require('./loaders');

const startServer = async (port) => {
  const app = express();
  loaders(app);
  app.listen(port);
};

startServer(parseInt(config.port || '8000', 10));