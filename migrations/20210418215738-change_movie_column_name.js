'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.renameColumn('movies', 'releaseDate', 'release_date');
  },

  down: async (queryInterface, Sequelize) => {
    queryInterface.renameColumn('movies', 'release_date', 'releaseDate');
  }
};
